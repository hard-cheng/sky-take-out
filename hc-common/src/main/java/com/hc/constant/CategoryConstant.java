package com.hc.constant;

/**
 * @ClassName: CategoryConstant
 * @Description: 状态常量，套餐或者菜品
 * @Author: Hard_cheng
 * @Date: 2023/9/28 1:22
 * @Version: 1.0
 */
public class CategoryConstant {
    //启用
    public static final Integer SETMEAL_CATEGORY = 2;

    //禁用
    public static final Integer DISH_CATEGORY = 1;
}
