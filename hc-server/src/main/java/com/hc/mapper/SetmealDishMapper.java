package com.hc.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hc.entity.SetmealDish;
import org.apache.ibatis.annotations.Mapper;

/**
 * @ClassName: SetmealDishMapper
 * @Description: TODO
 * @Author: Hard_cheng
 * @Date: 2023/9/23 16:40
 * @Version: 1.0
 */
@Mapper
public interface SetmealDishMapper extends BaseMapper<SetmealDish> {
}
