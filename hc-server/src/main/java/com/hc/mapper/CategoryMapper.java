package com.hc.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hc.entity.Category;
import org.apache.ibatis.annotations.Mapper;

/**
 * @ClassName: CategoryMapper
 * @Description: TODO
 * @Author: Hard_cheng
 * @Date: 2023/9/3 10:58
 * @Version: 1.0
 */
@Mapper
public interface CategoryMapper extends BaseMapper<Category> {
}
