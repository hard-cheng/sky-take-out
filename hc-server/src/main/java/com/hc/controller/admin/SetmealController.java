package com.hc.controller.admin;

import com.hc.constant.MessageConstant;
import com.hc.constant.StatusConstant;
import com.hc.dto.SetmealDTO;
import com.hc.dto.SetmealPageQueryDTO;
import com.hc.entity.Dish;
import com.hc.entity.Setmeal;
import com.hc.entity.SetmealDish;
import com.hc.exception.DeletionNotAllowedException;
import com.hc.exception.SetmealEnableFailedException;
import com.hc.result.PageResult;
import com.hc.result.Result;
import com.hc.service.DishService;
import com.hc.service.SetmealDishService;
import com.hc.service.SetmealService;
import com.hc.vo.SetmealVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * @ClassName: SetmealController
 * @Description: TODO
 * @Author: Hard_cheng
 * @Date: 2023/9/27 0:25
 * @Version: 1.0
 */
@RestController
@RequestMapping("/admin/setmeal")
@Slf4j
@Api(tags = "套餐相关接口")
public class SetmealController {

    @Autowired
    private SetmealService setmealService;

    @Autowired
    private SetmealDishService setmealDishService;

    @Autowired
    private DishService dishService;

    /**
     * 套餐分类查询
     * @param setmealPageQueryDTO
     * @return
     */
    @GetMapping("page")
    @ApiOperation("套餐分类查询")
    public Result<PageResult> page (SetmealPageQueryDTO setmealPageQueryDTO){
        PageResult pageResult = setmealService.page(setmealPageQueryDTO);
        return Result.success(pageResult);
    }

    @PostMapping
    @ApiOperation("新增套餐")
    public Result save(@RequestBody SetmealDTO setmealDTO){
        Setmeal setmeal = new Setmeal();
        BeanUtils.copyProperties(setmealDTO,setmeal);
        setmealService.saveOrUpdate(setmeal);
        setmealDTO.getSetmealDishes().forEach(sd -> {
            SetmealDish setmealDish = new SetmealDish();
            BeanUtils.copyProperties(sd,setmealDish);
            setmealDish.setSetmealId(setmeal.getId());
            setmealDishService.save(setmealDish);
        });
        return Result.success();
    }

    @PutMapping
    @ApiOperation("修改套餐")
    public Result update(@RequestBody SetmealDTO setmealDTO){
        Setmeal setmeal = new Setmeal();
        BeanUtils.copyProperties(setmealDTO,setmeal);
        setmealService.saveOrUpdate(setmeal);
        //删除后重新插入
        Map<String,Object> setmealDishServiceMap = new HashMap<>();
        setmealDishServiceMap.put("setmeal_id", setmeal.getId());
        setmealDishService.removeByMap(setmealDishServiceMap);
        setmealDTO.getSetmealDishes().forEach(sd -> {
            SetmealDish setmealDish = new SetmealDish();
            BeanUtils.copyProperties(sd,setmealDish);
            setmealDish.setSetmealId(setmeal.getId());
            setmealDishService.save(setmealDish);
        });
        return Result.success();
    }

    @GetMapping("{id}")
    @ApiOperation("根据id查询套餐")
    public Result<SetmealVO> selectSetmealVoById(@PathVariable String id){
        //根据id获取套餐数据
        Setmeal setmeal = setmealService.getById(id);

        //获取套餐关联菜品
        Map<String,Object> setmealDishServiceMap = new HashMap<>();
        setmealDishServiceMap.put("setmeal_id", setmeal.getId());
        List<SetmealDish> setmealDishes = setmealDishService.listByMap(setmealDishServiceMap);
        //拷贝数据
        SetmealVO setmealVO = new SetmealVO();
        BeanUtils.copyProperties(setmeal,setmealVO);
        if (setmealDishes != null && setmealDishes.size()>0) {
            setmealVO.setSetmealDishes(setmealDishes);
        }
        return Result.success(setmealVO);
    }

    @PostMapping("/status/{status}")
    @ApiOperation("套餐起售、停售")
    public Result startOrStop(@PathVariable Integer status,Long id){
        //查询套餐内菜品是否停售
        Map<String,Object> map = new HashMap<>();
        map.put("setmeal_id",id);
        List<SetmealDish> setmealDishes = setmealDishService.listByMap(map);
        setmealDishes.forEach(setmealDish -> {
            Dish dish = dishService.getById(setmealDish.getDishId());
            if (Objects.equals(dish.getStatus(), StatusConstant.DISABLE)) {
                //有菜品停售则套餐不能起售
                throw new SetmealEnableFailedException(MessageConstant.SETMEAL_ENABLE_FAILED);
            }
        });
        Setmeal setmeal = Setmeal.builder()
                .status(status)
                .id(id)
                .build();
        setmealService.updateById(setmeal);
        return Result.success();
    }

    @DeleteMapping
    @ApiOperation("批量删除套餐")
    public Result delete(@RequestParam List<Long> ids){
        ids.forEach(id ->{
            Setmeal setmeal = setmealService.getById(id);
            if (Objects.equals(setmeal.getStatus(), StatusConstant.ENABLE)) {
                throw new DeletionNotAllowedException(MessageConstant.SETMEAL_ON_SALE);
            }else {
                setmealService.removeByIds(ids);
            }
        });
        return Result.success();
    }

}
