package com.hc.controller.admin;


import com.hc.constant.StatusConstant;
import com.hc.dto.DishDTO;
import com.hc.dto.DishPageQueryDTO;
import com.hc.entity.Dish;
import com.hc.entity.Setmeal;
import com.hc.entity.SetmealDish;
import com.hc.result.PageResult;
import com.hc.result.Result;
import com.hc.service.DishService;
import com.hc.service.SetmealDishService;
import com.hc.service.SetmealService;
import com.hc.vo.DishVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @ClassName: DishController
 * @Description: 菜品控制类
 * @Author: Hard_cheng
 * @Date: 2023/9/16 0:17
 * @Version: 1.0
 */
@RestController
@RequestMapping("/admin/dish")
@Api(tags = "菜品接口")
@Slf4j
public class DishController {

    @Autowired
    private DishService dishService;

    @Autowired
    private SetmealDishService setmealDishService;

    @Autowired
    private SetmealService setmealService;

    @PutMapping
    @ApiOperation("修改菜品")
    public Result update(@RequestBody DishDTO dishDTO){
        dishService.updateDish(dishDTO);
        return Result.success();
    }

    @PostMapping
    @ApiOperation("新增菜品")
    public Result save(@RequestBody DishDTO dishDTO){
        dishService.saveWithFlavor(dishDTO);
        return Result.success();
    }

    @GetMapping("page")
    @ApiOperation("菜品分类查询")
    public Result<PageResult> page(DishPageQueryDTO dishPageQueryDTO){
        PageResult pageResult = dishService.page(dishPageQueryDTO);
        return Result.success(pageResult);
    }

    @DeleteMapping
    @ApiOperation("批量删除菜品")
    public Result delete(@RequestParam List<Long> ids){
        dishService.deleteByIds(ids);
        return Result.success();
    }


    /**
     * 根据id查询菜品
     * @param id
     * @return dishVO
     */
    @GetMapping("{id}")
    @ApiOperation("根据id查询菜品")
    public  Result<DishVO> selectDishById(@PathVariable Long id){
        DishVO dishVO = dishService.selectDishById(id);
        return Result.success(dishVO);
    }

    @GetMapping("list")
    @ApiOperation("根据分类id查询菜品")
    public Result<List<Dish>> list(Long categoryId){
        Map<String,Object> smap = new HashMap<>();
        smap.put("category_id",categoryId);
        smap.put("status", StatusConstant.ENABLE);
        List<Dish> list = dishService.listByMap(smap);
        return Result.success(list);
    }
    @PostMapping("/status/{status}")
    @ApiOperation("菜品起售、停售")
    public Result startOrStop(@PathVariable Integer status,Long id){
        //停售菜品
        Dish dish = Dish.builder()
                .status(status)
                .id(id)
                .build();
        dishService.updateById(dish);
        //查询是否关联套餐，如果关联则套餐一起禁用
        Map<String,Object> dishidmap = new HashMap<>();
        dishidmap.put("dish_id",id);
        List<SetmealDish> setmealDishes = setmealDishService.listByMap(dishidmap);
        if (setmealDishes.size()>0){
            setmealDishes.forEach(setmealDish -> {
                Setmeal setmeal = Setmeal.builder()
                        .status(StatusConstant.DISABLE)
                        .id(setmealDish.getSetmealId())
                        .build();
                setmealService.updateById(setmeal);
            });
        }
        return Result.success();
    }
}
