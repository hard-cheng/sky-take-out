package com.hc.handler;

import com.hc.constant.MessageConstant;
import com.hc.exception.BaseException;
import com.hc.result.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;

/**
 * 全局异常处理器，处理项目中抛出的业务异常
 */
@RestControllerAdvice
@Slf4j
public class GlobalExceptionHandler {

    /**
     * 捕获业务异常
     * @param ex
     * @return
     */
    @ExceptionHandler()
    public Result exceptionHandler(BaseException ex){
        log.error("异常信息：{}", ex.getMessage());
        return Result.error(ex.getMessage());
    }

    /**
     * 捕获 SQL 异常，处理并返回自定义错误结果
     * @param ex 抛出的 SQL 异常
     * @return 包含错误信息的自定义结果
     */
    @ExceptionHandler(SQLException.class)
    public Result handleSqlException(SQLException ex) {
//        log.error("SQL 异常：{}", ex.getMessage());

        if (ex instanceof SQLIntegrityConstraintViolationException) {
            String errorMessage = ex.getMessage();

            // 检查异常信息中是否包含 "Duplicate entry"
            if (errorMessage.contains("Duplicate entry")) {
                // 提取重复的值（'123'），并将它包含在自定义错误信息中
                String duplicateValue = errorMessage.split("'")[1];
                return Result.error("'" + duplicateValue + "' "+ MessageConstant.ALREADY_EXISTS);
            }

        }

        // 非 SQL 异常，返回通用错误信息
        return Result.error(MessageConstant.UNKNOWN_ERROR);
    }

}
