package com.hc.handler;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import com.hc.constant.AutoFillConstant;
import com.hc.context.BaseContext;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

/**
 * @ClassName: MyBatisAutoHandle
 * @Description: TODO
 * @Author: Hard_cheng
 * @Date: 2023/9/6 22:27
 * @Version: 1.0
 */
@Component
@Slf4j
public class MyBatisAutoHandle implements MetaObjectHandler {

    /**
     * 插入自动填充
     * @param metaObject
     */
    @Override
    public void insertFill(MetaObject metaObject) {
        metaObject.setValue(AutoFillConstant.CREATE_TIME, LocalDateTime.now());
        metaObject.setValue(AutoFillConstant.UPDATE_TIME, LocalDateTime.now());
        metaObject.setValue(AutoFillConstant.CREATE_USER, BaseContext.getCurrentId());
        metaObject.setValue(AutoFillConstant.UPDATE_USER, BaseContext.getCurrentId());
    }

    /**
     * 更新自动填充
     * @param metaObject
     */
    @Override
    public void updateFill(MetaObject metaObject) {
        metaObject.setValue(AutoFillConstant.UPDATE_TIME, LocalDateTime.now());
        metaObject.setValue(AutoFillConstant.UPDATE_USER, BaseContext.getCurrentId());
    }
}
