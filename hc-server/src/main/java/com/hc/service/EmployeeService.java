package com.hc.service;

import com.hc.dto.EmployeeDTO;
import com.hc.dto.EmployeeLoginDTO;
import com.hc.dto.EmployeePageQueryDTO;
import com.hc.entity.Employee;
import com.hc.result.PageResult;



public interface EmployeeService {

    /**
     * 员工登录
     * @param employeeLoginDTO
     * @return
     */
    Employee login(EmployeeLoginDTO employeeLoginDTO);

    /**
     * 新增员工
     * @param employeeDTO
     */
    void save(EmployeeDTO employeeDTO);

    /**
     * 分页查询
     * @param employeePageQueryDTO
     * @return
     */
    PageResult pageQuer(EmployeePageQueryDTO employeePageQueryDTO);

    /**
     * 启用禁用员工
     * @param status
     * @param id
     */
    void startorstop(Integer status, Long id);

    /**
     * 根据id查询员工
     * @param id
     * @return
     */
    Employee selectById(Long id);

    /**
     * 更新员工信息
     * @param employeeDTO
     */
    void updataEmployee(EmployeeDTO employeeDTO);
}
