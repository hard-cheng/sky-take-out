package com.hc.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hc.entity.SetmealDish;
import com.hc.mapper.SetmealDishMapper;
import com.hc.service.SetmealDishService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @ClassName: SetmealDishServiceImpl
 * @Description: TODO
 * @Author: Hard_cheng
 * @Date: 2023/9/27 23:42
 * @Version: 1.0
 */
@Service
@Slf4j
public class SetmealDishServiceImpl extends ServiceImpl<SetmealDishMapper, SetmealDish> implements SetmealDishService {

}
