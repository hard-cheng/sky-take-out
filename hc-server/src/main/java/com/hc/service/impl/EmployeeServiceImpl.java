package com.hc.service.impl;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.hc.constant.MessageConstant;
import com.hc.constant.PasswordConstant;
import com.hc.constant.StatusConstant;
import com.hc.context.BaseContext;
import com.hc.dto.EmployeeDTO;
import com.hc.dto.EmployeeLoginDTO;
import com.hc.dto.EmployeePageQueryDTO;
import com.hc.entity.Employee;
import com.hc.exception.AccountLockedException;
import com.hc.exception.AccountNotFoundException;
import com.hc.exception.PasswordErrorException;
import com.hc.mapper.EmployeeMapper;
import com.hc.result.PageResult;
import com.hc.service.EmployeeService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.DigestUtils;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

@Service
@Slf4j
public class EmployeeServiceImpl implements EmployeeService {

    @Autowired
    private EmployeeMapper employeeMapper;

    /**
     * 员工登录
     *
     * @param employeeLoginDTO
     * @return
     */
    public Employee login(EmployeeLoginDTO employeeLoginDTO) {
        String username = employeeLoginDTO.getUsername();
        String password = employeeLoginDTO.getPassword();

        //1、根据用户名查询数据库中的数据
        Employee employee = employeeMapper.getByUsername(username);

        //2、处理各种异常情况（用户名不存在、密码不对、账号被锁定）
        if (employee == null) {
            //账号不存在
            throw new AccountNotFoundException(MessageConstant.ACCOUNT_NOT_FOUND);
        }

        //密码比对
        // 密码md5加密处理
        password = DigestUtils.md5DigestAsHex(password.getBytes());
        if (!password.equals(employee.getPassword())) {
            //密码错误
            throw new PasswordErrorException(MessageConstant.PASSWORD_ERROR);
        }

        if (employee.getStatus() == StatusConstant.DISABLE) {
            //账号被锁定
            throw new AccountLockedException(MessageConstant.ACCOUNT_LOCKED);
        }

        //3、返回实体对象
        return employee;
    }

    /**
     * 新增员工
     * @param employeeDTO
     */
    public void save(EmployeeDTO employeeDTO) {
        Employee employee = new Employee();

        //对象拷贝
        BeanUtils.copyProperties(employeeDTO,employee);
        //设置账号状态
        employee.setStatus(StatusConstant.ENABLE);
        //设置默认密码
        employee.setPassword(DigestUtils.md5DigestAsHex(PasswordConstant.DEFAULT_PASSWORD.getBytes()));

        // 设置创建时间和更新时间为当前时间
        employee.setCreateTime(LocalDateTime.now());
        employee.setUpdateTime(LocalDateTime.now());

        //设置创建人id和修改人id
        employee.setCreateUser(BaseContext.getCurrentId());
        employee.setUpdateUser(BaseContext.getCurrentId());


        employeeMapper.insert(employee);
        log.info(employee.getId().toString());
    }

    /**
     * 分页查询
     * @param employeePageQueryDTO
     * @return
     */
    public PageResult pageQuer(EmployeePageQueryDTO employeePageQueryDTO) {
//        PageHelper.startPage(employeePageQueryDTO.getPage(),employeePageQueryDTO.getPageSize());
//        Page<Employee> page = employeeMapper.pageQuery(employeePageQueryDTO);
//        long total = page.getTotal();
//        List<Employee> records = page.getResult();
//
//        return new PageResult(total,records);
        QueryWrapper<Employee> queryWrapper = new QueryWrapper<>();//条件构造器
        if (employeePageQueryDTO.getName() != null && !Objects.equals(employeePageQueryDTO.getName(), "")) {
            queryWrapper.like("name", employeePageQueryDTO.getName());//模糊查询Like
        }
        Page<Employee> page = new Page(employeePageQueryDTO.getPage(), employeePageQueryDTO.getPageSize());//分页插件

        Page<Employee> userIPage  = employeeMapper.selectPage(page, queryWrapper);
        long total = userIPage.getTotal();
        List<Employee> records = userIPage.getRecords();
        return new PageResult(total,records);
    }

     /**
     * 启用禁用员工
     * @param status
     * @param id
     */
    public void startorstop(Integer status, Long id) {
        Employee employee = Employee.builder()
                .status(status)
                .id(id)
                .build();
        employeeMapper.updateById(employee);
    }

    /**
     * 根据id查询员工
     *
     * @param id
     * @return
     */
    public Employee selectById(Long id) {
        Employee employee = employeeMapper.selectById(id);
        employee.setPassword("****");
        return employee;
    }

    /**
     * 更新员工信息
     * @param employeeDTO
     */
    public void updataEmployee(EmployeeDTO employeeDTO) {
        Employee employee = new Employee();
        BeanUtils.copyProperties(employeeDTO,employee);

        employeeMapper.updateById(employee);
    }
}
