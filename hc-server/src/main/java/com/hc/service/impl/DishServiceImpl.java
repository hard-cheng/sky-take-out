package com.hc.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hc.constant.MessageConstant;
import com.hc.constant.StatusConstant;
import com.hc.dto.DishDTO;
import com.hc.dto.DishPageQueryDTO;
import com.hc.entity.Dish;
import com.hc.entity.DishFlavor;
import com.hc.entity.SetmealDish;
import com.hc.exception.DeletionNotAllowedException;
import com.hc.mapper.CategoryMapper;
import com.hc.mapper.DishFlavorMapper;
import com.hc.mapper.DishMapper;
import com.hc.mapper.SetmealDishMapper;
import com.hc.result.PageResult;
import com.hc.service.DishService;
import com.hc.vo.DishVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;


import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @ClassName: DishServiceImpl
 * @Description: TODO
 * @Author: Hard_cheng
 * @Date: 2023/9/16 0:22
 * @Version: 1.0
 */
@Service
@Slf4j
public class DishServiceImpl extends ServiceImpl<DishMapper,Dish> implements DishService {

    @Autowired
    private DishMapper dishMapper;

    @Autowired
    private DishFlavorMapper dishFlavorMapper;

    @Autowired
    private SetmealDishMapper setmealDishMapper;

    @Autowired
    private CategoryMapper categoryMapper;

    /**
     * 修改菜品
     * @param dishDTO
     */
    public void updateDish(DishDTO dishDTO) {
        Dish dish = new Dish();
        BeanUtils.copyProperties(dishDTO,dish);
        dishMapper.updateById(dish);
        List<DishFlavor> flavors = dishDTO.getFlavors();

        //先删除后添加
        QueryWrapper<DishFlavor> dishFlavorQueryWrapper = new QueryWrapper<>();
        dishFlavorQueryWrapper.eq("dish_id",dish.getId());
        dishFlavorMapper.delete(dishFlavorQueryWrapper);
        if (flavors != null && flavors.size() > 0 ) {
            flavors.forEach(dishFlavor -> {
                dishFlavor.setDishId(dishDTO.getId());
                dishFlavorMapper.insert(dishFlavor);
            });
        }
    }

    /**
     * 新增菜品
     * @param dishDTO
     */
    @Transactional
    public void saveWithFlavor(DishDTO dishDTO) {
        Dish dish = new Dish();
        BeanUtils.copyProperties(dishDTO,dish);

        dishMapper.insert(dish);

        Long dishId = dish.getId();

        List<DishFlavor> flavors = dishDTO.getFlavors();

        if (flavors != null && flavors.size() > 0 ) {
            flavors.forEach(dishFlavor1 -> {
                dishFlavor1.setDishId(dishId);
                dishFlavorMapper.insert(dishFlavor1);
            });
//            List<DishFlavor> newFlavors = flavors.stream()
//                    .map(dishFlavor -> {
//                        BeanUtils.copyProperties(dishFlavor1, dishFlavor);
//                        dishFlavor1.setDishId(dishId);
//                        return dishFlavor1;
//                    })
//                    .collect(Collectors.toList());

        }

    }

    /**
     * 菜品分类查询
     * @param dishPageQueryDTO
     * @return
     */
    public PageResult page(DishPageQueryDTO dishPageQueryDTO) {
        IPage<DishVO> page = new Page(dishPageQueryDTO.getPage(), dishPageQueryDTO.getPageSize());
        Page<DishVO> page1 = dishMapper.selectPageVo(page,dishPageQueryDTO);
        return new PageResult(page1.getTotal(),page1.getRecords());
    }

    /**
     * 根据ids批量删除菜品
     * @param ids
     */
    public void deleteByIds(List<Long> ids) {
        //菜品在售时不能删除
        ids.forEach(id ->{
            Dish dish = dishMapper.selectById(id);
            if (dish.getStatus() == StatusConstant.ENABLE) {
                throw new DeletionNotAllowedException(MessageConstant.DISH_ON_SALE);
            }
        });
        //菜品关联套餐时不能删除
        List<SetmealDish> setmealDishes = setmealDishMapper.selectBatchIds(ids);
        if (setmealDishes != null && setmealDishes.size() > 0) {
            throw new DeletionNotAllowedException(MessageConstant.DISH_BE_RELATED_BY_SETMEAL);
        }
        //删除菜品与关联的口味表
        ids.forEach(id ->{
            dishMapper.deleteById(id);
            Map<String,Object> dmap = new HashMap<>();
            dmap.put("dish_id",id);
            dishFlavorMapper.deleteByMap(dmap);
        });
    }

    /**
     * 根据id查询菜品
     * @param id
     * @return
     */
    public DishVO selectDishById(Long id) {
        Dish dish = dishMapper.selectById(id);
        Map<String,Object> selectMap = new HashMap<>();
        selectMap.put("dish_id",dish.getId());
        DishVO dishVO = DishVO.builder()
                .categoryName(categoryMapper.selectById(dish.getCategoryId()).getName())
                .flavors(dishFlavorMapper.selectByMap(selectMap))
                .build();
        BeanUtils.copyProperties(dish,dishVO);
        return dishVO;
    }
}
