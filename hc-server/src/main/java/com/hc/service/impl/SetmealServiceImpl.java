package com.hc.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hc.dto.SetmealPageQueryDTO;
import com.hc.entity.Setmeal;
import com.hc.mapper.SetmealMapper;
import com.hc.result.PageResult;
import com.hc.service.SetmealService;
import com.hc.vo.SetmealVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @ClassName: SetmealServiceImpl
 * @Description: TODO
 * @Author: Hard_cheng
 * @Date: 2023/9/27 0:27
 * @Version: 1.0
 */
@Service
@Slf4j
public class SetmealServiceImpl extends ServiceImpl<SetmealMapper,Setmeal> implements SetmealService {

    @Autowired
    private SetmealMapper setmealMapper;

    /**
     * 套餐分类查询
     * @param setmealPageQueryDTO
     * @return
     */
    public PageResult page(SetmealPageQueryDTO setmealPageQueryDTO) {
        IPage<SetmealVO> setmealVOIPage = new Page(setmealPageQueryDTO.getPage(),setmealPageQueryDTO.getPageSize());
        Page<SetmealVO> page = setmealMapper.selectPageVo(setmealVOIPage,setmealPageQueryDTO);
        return new PageResult(page.getTotal(),page.getRecords());
    }
}
