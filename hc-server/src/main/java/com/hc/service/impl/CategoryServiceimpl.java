package com.hc.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.hc.constant.CategoryConstant;
import com.hc.constant.MessageConstant;
import com.hc.constant.StatusConstant;
import com.hc.context.BaseContext;
import com.hc.dto.CategoryDTO;
import com.hc.dto.CategoryPageQueryDTO;
import com.hc.entity.Category;
import com.hc.entity.Dish;
import com.hc.entity.Setmeal;
import com.hc.exception.DeletionNotAllowedException;
import com.hc.mapper.CategoryMapper;
import com.hc.mapper.DishMapper;
import com.hc.mapper.SetmealMapper;
import com.hc.result.PageResult;
import com.hc.service.CategoryService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * @ClassName: CategoryServiceimpl
 * @Description: TODO
 * @Author: Hard_cheng
 * @Date: 2023/9/3 10:53
 * @Version: 1.0
 */
@Service
@Slf4j
public class CategoryServiceimpl implements CategoryService {

    @Autowired
    private CategoryMapper categoryMapper;
    @Autowired
    private DishMapper dishMapper;
    @Autowired
    private SetmealMapper setmealMapper;
    public void save(CategoryDTO categoryDTO) {
        Category category = new Category();
        BeanUtils.copyProperties(categoryDTO,category);
        // 设置创建时间和更新时间为当前时间
        category.setCreateTime(LocalDateTime.now());
        category.setUpdateTime(LocalDateTime.now());

        //设置创建人id和修改人id
        category.setCreateUser(BaseContext.getCurrentId());
        category.setUpdateUser(BaseContext.getCurrentId());

        //默认禁用
        category.setStatus(StatusConstant.DISABLE);

        categoryMapper.insert(category);
    }

    /**
     * 分类分页查询
     * @param categoryPageQueryDTO
     * @return
     */
    public PageResult pageQuer(CategoryPageQueryDTO categoryPageQueryDTO) {
        QueryWrapper<Category> queryWrapper = new QueryWrapper<>();//条件构造器
        Page<Category> page = new Page(categoryPageQueryDTO.getPage(), categoryPageQueryDTO.getPageSize());//分页插件

        if (categoryPageQueryDTO.getName() != null && !Objects.equals(categoryPageQueryDTO.getName(), "")) {
            queryWrapper.like("name", categoryPageQueryDTO.getName());//模糊查询Like

        }
        Page<Category> categoryIPage  = categoryMapper.selectPage(page, queryWrapper);
        long total = categoryIPage.getTotal();

        List<Category> records = categoryIPage.getRecords();
        return new PageResult(total,records);
    }

    /**
     * 启用禁用分类
     * @param status
     * @param id
     */
    public void startorstop(Integer status, Long id) {
        Category category = Category.builder()
                .status(status)
                .id(id)
                .build();
        categoryMapper.updateById(category);
    }

    /**
     * 根据id删除分类
     * @param id
     */
    public void delcategorybyid(Long id) {
        QueryWrapper<Dish> Wrapper = new QueryWrapper<>();//条件构造器
        Wrapper.gt("category_id",id);

        QueryWrapper<Setmeal> SWrapper = new QueryWrapper<>();//条件构造器
        SWrapper.gt("category_id",id);

        Category category1 = categoryMapper.selectById(id);
        if (dishMapper.selectCount(Wrapper) > 0 && Objects.equals(category1.getType(), CategoryConstant.DISH_CATEGORY)) {
            //当前分类下有菜品不能删除
            throw new DeletionNotAllowedException(MessageConstant.CATEGORY_BE_RELATED_BY_DISH);
        }
        if (setmealMapper.selectCount(SWrapper) > 0 && Objects.equals(category1.getType(), CategoryConstant.SETMEAL_CATEGORY)) {
            //当前分类下有套餐不能删除
            throw new DeletionNotAllowedException(MessageConstant.CATEGORY_BE_RELATED_BY_SETMEAL);
        }
        Category category = Category.builder()
                .id(id)
                .build();
        categoryMapper.deleteById(category);
    }

    /**
     * 更新分类信息
     * @param categoryDTO
     */
    public void updatecategory(CategoryDTO categoryDTO) {
        Category category = Category.builder()
                .updateTime(LocalDateTime.now())
                .updateUser(BaseContext.getCurrentId())
                .build();

        BeanUtils.copyProperties(categoryDTO,category);

        categoryMapper.updateById(category);

    }

    /**
     * 根据类型查询分类
     *
     * @param type
     * @return
     */
    public List<Category> selectByType(Integer type) {
        Map<String,Object> map = new HashMap<>();
        map.put("type",type);
        map.put("status",StatusConstant.ENABLE);

        QueryWrapper<Category> categoryQueryWrapper = new QueryWrapper<>();
        categoryQueryWrapper.allEq(map,false)
                .orderByAsc("sort")
                .orderByDesc("create_time");
        return categoryMapper.selectList(categoryQueryWrapper);
    }
}
