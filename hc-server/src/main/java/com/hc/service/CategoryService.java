package com.hc.service;

import com.hc.dto.CategoryDTO;
import com.hc.dto.CategoryPageQueryDTO;
import com.hc.entity.Category;
import com.hc.result.PageResult;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @ClassName: CategoryService
 * @Description: TODO
 * @Author: Hard_cheng
 * @Date: 2023/9/3 10:52
 * @Version: 1.0
 */
@Service
public interface CategoryService {
    /**
     * 新增分类
     * @param categoryDTO
     */
    void save(CategoryDTO categoryDTO);

    /**
     * 分类分页查询
     * @param categoryPageQueryDTO
     * @return
     */
    PageResult pageQuer(CategoryPageQueryDTO categoryPageQueryDTO);

    /**
     * 启用禁用分类
     * @param status
     * @param id
     */
    void startorstop(Integer status, Long id);

    /**
     * 根据id删除分类
     * @param id
     */
    void delcategorybyid(Long id);

    /**
     * 修改分类
     * @param categoryDTO
     * @return
     */
    void updatecategory(CategoryDTO categoryDTO);

    /**
     * 根据类型查询分类
     * @param type
     * @return
     */
    List<Category> selectByType(Integer type);
}
