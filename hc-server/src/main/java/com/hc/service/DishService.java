package com.hc.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hc.dto.DishDTO;
import com.hc.dto.DishPageQueryDTO;
import com.hc.entity.Dish;
import com.hc.result.PageResult;
import com.hc.vo.DishVO;

import java.util.List;

/**
 * @ClassName: DishService
 * @Description: TODO
 * @Author: Hard_cheng
 * @Date: 2023/9/16 0:22
 * @Version: 1.0
 */

public interface DishService extends IService<Dish> {


    void updateDish(DishDTO dishDTO);

    void saveWithFlavor(DishDTO dishDTO);

    /**
     * 菜品分类查询
     * @param dishPageQueryDTO
     * @return
     */
    PageResult page(DishPageQueryDTO dishPageQueryDTO);

    /**
     * 根据ids批量删除菜品
     * @param ids
     */
    void deleteByIds(List<Long> ids);

    /**
     * 根据id查询菜品
     * @param id
     * @return
     */
    DishVO selectDishById(Long id);
}
