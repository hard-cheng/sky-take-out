package com.hc.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hc.entity.SetmealDish;

/**
 * @ClassName: SetmealDishService
 * @Description: TODO
 * @Author: Hard_cheng
 * @Date: 2023/9/27 0:27
 * @Version: 1.0
 */
public interface SetmealDishService extends IService<SetmealDish>{

}
