package com.hc.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hc.dto.SetmealPageQueryDTO;
import com.hc.entity.Setmeal;
import com.hc.result.PageResult;

/**
 * @ClassName: SetmealService
 * @Description: TODO
 * @Author: Hard_cheng
 * @Date: 2023/9/27 0:27
 * @Version: 1.0
 */
public interface SetmealService extends IService<Setmeal>{
    /**
     * 套餐分类查询
     * @param setmealPageQueryDTO
     * @return PageResult page
     */
    PageResult page(SetmealPageQueryDTO setmealPageQueryDTO);
}
